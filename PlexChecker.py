

#	Written by Gavin Fuller #	This attempts to connect to a Plex server. #	If no response is received it restarts the exe. #	This is for a very narrow and specific need. #	If you do not know how to use python or do not #	understand a part of this then do not use it. #	Conceivably it could be re-used very easily to #	preform just about anything if a locally or #	off site web application goes down. #	It's probably a good idea to set this file to read only #	especially if you are running it automatically, as otherwise #	a malicious attacker could conceivably change the strings #	found in the os.system() calls to do bad things. #	This program is free software: you can redistribute it and/or modify #	it under the terms of the GNU General Public License as published by #	the Free Software Foundation, either version 3 of the License, or #	(at your option) any later version. #	This program is distributed in the hope that it will be useful, #	but WITHOUT ANY WARRANTY; without even the implied warranty of #	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the #	GNU General Public License for more details. #	You should have received a copy of the GNU General Public License #	along with this program.  If not, see <https://www.gnu.org/licenses/>. #   TODO: Check for MacOS and BSD support. logging = True #If you would like the script to log events to a file. Default is True. computer = "192.168.1.100:32400" #IP address of the Plex server to be monitored. I recommend the local IP address, followed by the port, for example 192.168.1.1:32400, or whatever your Plex servers IP address is. maxRetries = 4 #If it fails to connect, the script will retry this many times. Counting starts at 0, so 4 would actually happen 5 times. sleepRetries = 12 #If it fails to connect, the script wait this many seconds before retrying again. Values of 0 will skip waiting entirely. import os import datetime from platform import python_version pyver = python_version() if int(pyver[0]) < 3: 	import httplib else: 	import http.client 	def checkInternet(): 	x = 0 	reply = False 	for x in range(maxRetries): 		if  reply == False: 			if logging: 				open("log.txt", "a").write("\n" + (datetime.datetime.now().strftime("%b %d, %Y")) +  " at " + (datetime.datetime.now().strftime("%I:%M:%S %p")) + " | testing connection to "+ computer) 				open("log.txt", "a").close() 						if int(pyver[0]) < 3: 				conn = httplib.HTTPConnection(computer, timeout=5) 			else: 				conn = http.client.HTTPConnection(computer, timeout=5) 						try: 				conn.request("HEAD", "/") 				conn.close() 				reply = True 							if logging: 					open("log.txt", "a").write("\n" + (datetime.datetime.now().strftime("%b %d, %Y")) +  " at " + (datetime.datetime.now().strftime("%I:%M:%S %p")) + " | reply is " + str(reply).lower()) 					open("log.txt", "a").close() 							except: 				conn.close() 								if logging: 					open("log.txt", "a").write("\n" + (datetime.datetime.now().strftime("%b %d, %Y")) +  " at " + (datetime.datetime.now().strftime("%I:%M:%S %p")) + " | failed to establish a connection, attempt " + str(1+x) + "/" + str(maxRetries)) 					open("log.txt", "a").close() 								if sleepRetries > 0.0: #Set this to 0.0 because I'm not entirely sure if python would store this as an int and therefore think that values between 0 and 1 were 0. 					import time 					time.sleep(sleepRetries) 	return reply def killPlex(): 	if os.name == 'nt': #For Windows 		os.system("taskkill /f /t /im Plex*") #/f forces, /t terminates any processes spawned from the application, and /im allows the use of the wildcard * 	else: 		os.system("pkill -9 Plex") #-9 sends a SIGKILL 			return True 	def startPlex(): 	if os.name == 'nt': #For Windows 		os.system("\"C:\Program Files (x86)\Plex\Plex Media Server\Plex Media Server.exe\"") #Default path to the media server exe 	else: 		os.system("service plexmediaserver start") #Put the path to plex media server, putting sudo on the beginning may be required. 	return True def restartPlex(): 	if logging: 		open("log.txt", "a").write("\n" + (datetime.datetime.now().strftime("%b %d, %Y")) +  " at " + (datetime.datetime.now().strftime("%I:%M:%S %p")) + " | restarting plex process") 		open("log.txt", "a").close() 		open("log.txt", "a").write("\n" + (datetime.datetime.now().strftime("%b %d, %Y")) +  " at " + (datetime.datetime.now().strftime("%I:%M:%S %p")) + " | killing plex process") 		open("log.txt", "a").close() 	killPlex() 	if logging: 		open("log.txt", "a").write("\n" + (datetime.datetime.now().strftime("%b %d, %Y")) +  " at " + (datetime.datetime.now().strftime("%I:%M:%S %p")) + " | starting plex process") 		open("log.txt", "a").close() 			startPlex() 		if logging: 		restartLogger() def restartLogger(): 	open("log.txt", "a").write(("\n" + (datetime.datetime.now().strftime("%b %d, %Y")) +  " at " + (datetime.datetime.now().strftime("%I:%M:%S %p")) + " | restarted plex process")) 	open("log.txt", "a").close() def successLogger(): 	open("log.txt", "a").write(("\n" + (datetime.datetime.now().strftime("%b %d, %Y")) +  " at " + (datetime.datetime.now().strftime("%I:%M:%S %p")) + " | successfully communicated with plex process")) 	open("log.txt", "a").close() def logStarter(): 	try: 		open("log.txt").close() 	except: 		open("log.txt", "w+").write("DATE & TIME                 | DETAILS\n===========================================================================") 		open("log.txt").close() 		def main():         if checkInternet() is False:                 restartPlex() 		        elif logging:                 successLogger() if logging:         logStarter() #Sets up the log file in the event that it is not present. while true: #Windows Scheduler isn't working so this just keeps the process running indefinitely. 	main() #Starts the main function. 	sleep(300) #Waits for 5 minutes.

#	Written by Gavin Fuller
#	This attempts to connect to a Plex server.
#	If no response is received it restarts the exe.
#	This is for a very narrow and specific need.
#	If you do not know how to use python or do not 
#	understand a part of this then do not use it.
#	Conceivably it could be re-used very easily to
#	preform just about anything if a locally or
#	off site web application goes down.

#	It's probably a good idea to set this file to read only
#	especially if you are running it automatically, as otherwise
#	a malicious attacker could conceivably change the strings
#	found in the os.system() calls to do bad things.

#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.

#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.

#	You should have received a copy of the GNU General Public License
#	along with this program.  If not, see <https://www.gnu.org/licenses/>.

#   TODO: Check for MacOS and BSD support.

logging = True #If you would like the script to log events to a file. Default is True.
computer = "192.168.1.100:32400" #IP address of the Plex server to be monitored. I recommend the local IP address, followed by the port, for example 192.168.1.1:32400, or whatever your Plex servers IP address is.
maxRetries = 4 #If it fails to connect, the script will retry this many times. Counting starts at 0, so 4 would actually happen 5 times.
sleepRetries = 12 #If it fails to connect, the script wait this many seconds before retrying again. Values of 0 will skip waiting entirely.

import os
import time
import datetime
from platform import python_version
pyver = python_version()
if int(pyver[0]) < 3:
	import httplib

else:
	import http.client
	
def checkInternet():
	x = 0
	reply = False
	for x in range(maxRetries):
		if  reply == False:
			if logging:
				open("log.txt", "a").write("\n" + (datetime.datetime.now().strftime("%b %d, %Y")) +  " at " + (datetime.datetime.now().strftime("%I:%M:%S %p")) + " | testing connection to "+ computer)
				open("log.txt", "a").close()
			
			if int(pyver[0]) < 3:
				conn = httplib.HTTPConnection(computer, timeout=5)

			else:
				conn = http.client.HTTPConnection(computer, timeout=5)
			
			try:
				conn.request("HEAD", "/")
				conn.close()
				reply = True
			
				if logging:
					open("log.txt", "a").write("\n" + (datetime.datetime.now().strftime("%b %d, %Y")) +  " at " + (datetime.datetime.now().strftime("%I:%M:%S %p")) + " | reply is " + str(reply).lower())
					open("log.txt", "a").close()
				
			except:
				conn.close()
				
				if logging:
					open("log.txt", "a").write("\n" + (datetime.datetime.now().strftime("%b %d, %Y")) +  " at " + (datetime.datetime.now().strftime("%I:%M:%S %p")) + " | failed to establish a connection, attempt " + str(1+x) + "/" + str(maxRetries))
					open("log.txt", "a").close()
				
				if sleepRetries > 0.0: #Set this to 0.0 because I'm not entirely sure if python would store this as an int and therefore think that values between 0 and 1 were 0.
					time.sleep(sleepRetries)

	return reply

def killPlex():
	if os.name == 'nt': #For Windows
		os.system("taskkill /f /t /im Plex*") #/f forces, /t terminates any processes spawned from the application, and /im allows the use of the wildcard *

	else:
		os.system("pkill -9 Plex") #-9 sends a SIGKILL
		
	return True
	
def startPlex():
	if os.name == 'nt': #For Windows
		os.system("\"C:\Program Files (x86)\Plex\Plex Media Server\Plex Media Server.exe\"") #Default path to the media server exe

	else:
		os.system("service plexmediaserver start") #Put the path to plex media server, putting sudo on the beginning may be required.

	return True

def restartPlex():
	if logging:
		open("log.txt", "a").write("\n" + (datetime.datetime.now().strftime("%b %d, %Y")) +  " at " + (datetime.datetime.now().strftime("%I:%M:%S %p")) + " | restarting plex process")
		open("log.txt", "a").close()
		open("log.txt", "a").write("\n" + (datetime.datetime.now().strftime("%b %d, %Y")) +  " at " + (datetime.datetime.now().strftime("%I:%M:%S %p")) + " | killing plex process")
		open("log.txt", "a").close()

	killPlex()

	if logging:
		open("log.txt", "a").write("\n" + (datetime.datetime.now().strftime("%b %d, %Y")) +  " at " + (datetime.datetime.now().strftime("%I:%M:%S %p")) + " | starting plex process")
		open("log.txt", "a").close()
		
	startPlex()
	
	if logging:
		restartLogger()

def restartLogger():
	open("log.txt", "a").write(("\n" + (datetime.datetime.now().strftime("%b %d, %Y")) +  " at " + (datetime.datetime.now().strftime("%I:%M:%S %p")) + " | restarted plex process"))
	open("log.txt", "a").close()

def successLogger():
	open("log.txt", "a").write(("\n" + (datetime.datetime.now().strftime("%b %d, %Y")) +  " at " + (datetime.datetime.now().strftime("%I:%M:%S %p")) + " | successfully communicated with plex process"))
	open("log.txt", "a").close()

def logStarter():
	try:
		open("log.txt").close()

	except:
		open("log.txt", "w+").write("DATE & TIME                 | DETAILS\n===========================================================================")
		open("log.txt").close()
		
def main():
        if checkInternet() is False:
                restartPlex()
		
        elif logging:
                successLogger()

if logging:
        logStarter() #Sets up the log file in the event that it is not present.

while True: #Windows Scheduler isn't working so this just keeps the process running indefinitely.
	main() #Starts the main function.
	time.sleep(300) #Waits for 5 minutes.
