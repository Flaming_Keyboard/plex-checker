AUTHOR
======
Written by Gavin Fuller

PYTHON VERSION
==============
| Python 2.7 | Python 3 |
| -------- | -------- |
| Yes   | Yes   |

OPERATING SYSTEMS
=================
| Windows | GNU/Linux | MacOS | BSD |
| -------- | -------- | -------- | -------- |
| Supported (tested on 8.1 and 10)  | Supported (tested on Ubuntu 16.04 LTS)   | Untested (should work)   | Untested (should work)   | 

README AND DISCLAIMER
=====================
This simple script attempts to connect to a plex server.
If no response is received it restarts the exe.
This is for a very narrow and specific need.
If you do not know how to use python or do not 
understand a part of the program then do not use it.
Conceivably it could be re-used very easily to
preform just about anything if a locally or
off site web application goes down.

SECURITY
========
It's probably a good idea to set this file to read only after
you finish configuring it for your setup, especially if you 
are running it automatically, as otherwise a malicious attacker
could conceivably change the strings found in the os.system()
calls to do bad things.

HOW TO USE
==========
Either run it directly in python IDLE or use the command

```python
python PlexChecker.py
```

Additionally, if you are using it in a Windows environment,
you can schedule it to run with task scheduler by pointing it to your
python.exe and supplying the PlexChecker.py as an argument.
The script has very little overhead, so setting it to run every
few minutes shouldn't make any perceptible difference in most
instances.

If you're on GNU/Linux then this either isn't a problem, or
you already know how to run and schedule a task.

TODO
====
- [x] ~~Windows support~~

- [x] ~~Linux support~~

- [ ] MacOS support

- [ ] BSD support

- [x] ~~Support for Python 2 and 3~~

- [x] ~~Add logging option~~


WHY
===
I wrote the script because I couldn't find anything similar, and
I figured it would be a good way to brush up on python.

NO, I MEAN WHY?!
================
My local media server has been crashing frequently, and currently
it is difficult to get to the root of the problem. So for the moment,
I've decided it's just easier to have it reboot the media server
automatically until the cause becomes clear.

LICENSE
=======
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.